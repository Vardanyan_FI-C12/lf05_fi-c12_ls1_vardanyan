
public class AusgabenFormatierungTest {

	public static void main(String[] args) {
		
		// Aufgabe 1: Zwei erdachte Sätze 
		System.out.println("Mein erster Satz!");
		System.out.println("Mein zweiter Satz!");
		
		//Aufgabe 1: Escape Symbole und Zeilenumbrüche
		System.out.print("Mein erster \"Satz!\"\n" + "Mein zweiter \"Satz!\"\n");

		// Aufgabe 1: Kommentare 
		
		// Hallo
		
		// Aufgabe 2: Muster ausgeben
		
		System.out.print(  "        *\n"        +
						   "       ***\n"       +
						   "      *****\n"      +
						   "     *******\n"     +
						   "    *********\n"    +
						   "   ***********\n"   +
						   "  *************\n"  +
						   "       ***      \n" +
						   "       ***       \n");
		
		// Aufgabe 3: 
		
		double d1 = 22.4234234;
		double d2 = 111.2222;
		double d3 = 4.0;
		double d4 = 1000000.551;
		double d5 = 97.34;
		
		System.out.printf("%.2f\n", d1);
		System.out.printf("%.2f\n", d2);
		System.out.printf("%.2f\n", d3);
		System.out.printf("%.2f\n", d4);
		System.out.printf("%.2f\n", d5);
		
	}

}
