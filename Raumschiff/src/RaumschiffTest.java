// A4.2.1 Quellcode verwenden *** 

public class RaumschiffTest {

	public static void main(String[] args) {
				
		// Ladungen der Raumschiffe
		
		// Ladung Kilgonen (Anfangszustand)
		Ladung ladungKilgonen1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladungKilgonen2 = new Ladung("Bat'leth Kilgonen Schwert", 200);
		
		// Ladung Romulaner
		Ladung ladungRomulaner1 = new Ladung("Borg-Schrott", 5);
		Ladung ladungRomulaner2 = new Ladung("Rote Materie", 2);
		Ladung ladungRomulaner3 = new Ladung("Plasma-Waffe", 50);
		
		// Ladung Vulkanier
		Ladung ladungVulkanier1 = new Ladung("Forschungssonde", 35);
		Ladung ladungVulkanier2 = new Ladung("Phontonentorpedo", 3);
		
		// Raumschiffe mit allen Spezifikationen (Anfangszustand)
		Raumschiff kilgonen = new Raumschiff();
		kilgonen.setPhotonentorpedoAnzahl(1);
		kilgonen.setEnergieversorgungInProzent(100);
		kilgonen.setSchildeInProzent(100);
		kilgonen.setHuelleInProzent(100);
		kilgonen.setLebenserhaltungssystemeInProzent(100);
		kilgonen.setSchiffsname("IKS Hegh'ta");
		kilgonen.setAndroidenAnzahl(2);
		Raumschiff romulaner = new Raumschiff();
		romulaner.setPhotonentorpedoAnzahl(2);
		romulaner.setEnergieversorgungInProzent(100);
		romulaner.setSchildeInProzent(100);
		romulaner.setHuelleInProzent(100);
		romulaner.setLebenserhaltungssystemeInProzent(100);
		romulaner.setSchiffsname("IRW Khazara");
		romulaner.setAndroidenAnzahl(2);
		Raumschiff vulkanier = new Raumschiff();
		vulkanier.setPhotonentorpedoAnzahl(0);
		vulkanier.setSchildeInProzent(80);
		vulkanier.setEnergieversorgungInProzent(80);
		vulkanier.setHuelleInProzent(50);
		vulkanier.setLebenserhaltungssystemeInProzent(100);
		vulkanier.setSchiffsname("Ni'Var");
		vulkanier.setAndroidenAnzahl(5);
		
		// Hinzufuegen der Ladungen zu den jeweiligen Raumschiffen
		
		// Kilgonen
		kilgonen.addLadung(ladungKilgonen1);
		kilgonen.addLadung(ladungKilgonen2);
		// Romulaner
		romulaner.addLadung(ladungRomulaner1);
		romulaner.addLadung(ladungRomulaner2);
		romulaner.addLadung(ladungRomulaner3);
		// Vulkanier
		vulkanier.addLadung(ladungVulkanier1);
		
		// Die Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner.
		kilgonen.phontonentorpedoSchiessen(romulaner);
		
		// Die Romulaner schie�en mit der Phaserkanone zur�ck.
		romulaner.phaserkanoneSchiessen(kilgonen);
		
		// Die Vulkanier senden eine Nachricht an Alle �Gewalt ist nicht logisch�.
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch\n");
		
		// Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus
		kilgonen.zustandRaumschiff();
		kilgonen.ladungsverzeichnisAusgeben();
		
		// Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein.
		vulkanier.reparaturDurchfuehren(true, true, true, 5);
		
		// Die Vulkanier verladen Ihre Ladung �Photonentorpedos� in die Torpedor�hren Ihres Raumschiffes und r�umen das Ladungsverzeichnis auf. 
		vulkanier.phontonentorpedosLaden(vulkanier, ladungVulkanier2, 3);
		vulkanier.ladungsverzeichnisAufraeumen();
		
		// Die Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner.
		kilgonen.phontonentorpedoSchiessen(romulaner);
		
		// Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
		// Zustand der Raumschiffe (Endzustand)
		kilgonen.zustandRaumschiff();
		romulaner.zustandRaumschiff();
		vulkanier.zustandRaumschiff();
		
		// Ladungen (Endzustand)
		kilgonen.ladungsverzeichnisAusgeben();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.ladungsverzeichnisAusgeben();
		
		// Geben Sie den broadcastKommunikator aus. 
		System.out.println(kilgonen.eintraegeLogbuchZurueckgeben());
		System.out.println(romulaner.eintraegeLogbuchZurueckgeben());
		System.out.println(vulkanier.eintraegeLogbuchZurueckgeben());
	}

}
