import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	public Raumschiff() {
		
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname, int androidenAnzahl) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public void addLadung(Ladung neueLadung) {

		this.ladungsverzeichnis.add(neueLadung);
	}

	public void phontonentorpedoSchiessen(Raumschiff r) {
		System.out.println(schiffsname);
		System.out.println("================");
		if (photonentorpedoAnzahl == 0) {
			System.out.println("Keine Phontonentorpedos gefunden!\n");
			r.nachrichtAnAlle("-=*Click*=-\n");
		} else {
			r.nachrichtAnAlle("Phontonentorpedo abgeschossen\n");
			photonentorpedoAnzahl -= 1;
			r.treffer(r);
		}
	}

	public void phontonentorpedosLaden(Raumschiff r, Ladung l, int anzahlTorpedos) {

		String phontonentorpedo = "Phontonentorpedo";

		System.out.println(schiffsname);
		System.out.println("================");
		if (l.getBezeichnung() != phontonentorpedo) {
			System.out.println("Keine Phontonentorpedos gefunden!\n");
			nachrichtAnAlle("-=*Click*=-\n");
		}
		if (l.getBezeichnung() == phontonentorpedo) {
			if (anzahlTorpedos > l.getMenge()) {
				anzahlTorpedos = l.getMenge();
			}
			if (l.getMenge() != 0) {
				System.out.println(anzahlTorpedos + " Phontonentorpedo(s) eingesetzt\n");
				r.photonentorpedoAnzahl += anzahlTorpedos;
				l.setMenge(anzahlTorpedos -= l.getMenge());
			}
		}
	}

	public void phaserkanoneSchiessen(Raumschiff r) {
		System.out.println(schiffsname);
		System.out.println("================");
		if (r.energieversorgungInProzent < 50) {
			r.nachrichtAnAlle("-=*Click*=-\n");
		} else {
			r.nachrichtAnAlle("Phaserkanone abgeschossen\n");
			r.energieversorgungInProzent -= 50;
			r.treffer(r);
		}
	}

	private void treffer(Raumschiff r) {
		r.nachrichtAnAlle(r.schiffsname + " wurde getroffen!\n");
		r.schildeInProzent -= 50;

		if (r.schildeInProzent < 0) {
			r.schildeInProzent = 0;
		}

		if (r.schildeInProzent == 0) {
			r.huelleInProzent -= 50;
			r.energieversorgungInProzent -= 50;
		}

		if (r.huelleInProzent < 0) {
			r.huelleInProzent = 0;
		}

		if (r.energieversorgungInProzent < 0) {
			r.energieversorgungInProzent = 0;
		}

		if (r.huelleInProzent == 0) {
			r.nachrichtAnAlle("Die Lebenserhaltungssysteme wurden zerstoert\n");
		}
	}

	public void nachrichtAnAlle(String message) {

		System.out.println(message);
		broadcastKommunikator.add(message);

	}

	public ArrayList<String> eintraegeLogbuchZurueckgeben() {

		System.out.println("Logbucheintraege von " + schiffsname);
		System.out.println("====================================");
		return broadcastKommunikator;
	}

	public void reparaturDurchfuehren(boolean schutzschilde, 
									  boolean energieversorgung, 
									  boolean schiffshuelle,
									  int anzahlDroiden) {

	
		Random rand = new Random();
		int bound = 100;
		int randomNumber = rand.nextInt(bound);
		
		int repairStructureAmount = 0;

		if (schutzschilde == true) {
			repairStructureAmount++;
		}

		if (energieversorgung == true) {
			repairStructureAmount++;
		}

		if (schiffshuelle == true) {
			repairStructureAmount++;
		}

		int repairPercent = randomNumber * anzahlDroiden / repairStructureAmount;

		if (schutzschilde == true) {
			schildeInProzent += repairPercent;
			if (schildeInProzent > 100) {
				schildeInProzent = 100;
			}
		}

		if (energieversorgung == true) {
			energieversorgungInProzent += repairPercent;
			if (energieversorgungInProzent > 100) {
				energieversorgungInProzent = 100;
			}
		}

		if (schiffshuelle == true) {
			huelleInProzent += repairPercent;
			if (huelleInProzent > 100) {
				huelleInProzent = 100;
			}
		}

	}

	public void zustandRaumschiff() {
		System.out.println("Zustand des Raumschiffs");
		System.out.println("=======================");
		System.out.println("Raumschiff: " + schiffsname);
		System.out.println("Photonentorpedo Anzahl: " + photonentorpedoAnzahl);
		System.out.println("Energieversorgung in Prozent: " + energieversorgungInProzent);
		System.out.println("Schilde in Prozent: " + schildeInProzent);
		System.out.println("Huelle in Prozent: " + huelleInProzent);
		System.out.println("Lebenserhaltungssysteme in Prozent: " + lebenserhaltungssystemeInProzent);
		System.out.println("Androiden Anzahl: " + androidenAnzahl + "\n");
	}

	public void ladungsverzeichnisAusgeben() {
		System.out.println("Ladung von " + schiffsname);
		System.out.println("==========================");
		for (Ladung e : ladungsverzeichnis) {
			System.out.println(e.toString());
		}
	}

	public void ladungsverzeichnisAufraeumen() {

		ladungsverzeichnis.removeIf(element -> (element.getMenge() == 0));
	}

}
