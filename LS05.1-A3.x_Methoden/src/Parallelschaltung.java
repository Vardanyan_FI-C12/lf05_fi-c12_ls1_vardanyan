
public class Parallelschaltung {

	public static void main(String[] args) {	
		// Berechnung Ersatzwiderstand einer Parallelschaltung
		double result = parallelschaltung(20.0, 40.0);
		System.out.println("Der Ersatzwiderstand der Parallelschaltung betr�gt: " + result);
	}

	// Methode f�r die Berechnung des Ersatzwiderstands von r1 und r2 einer Parallelschaltung
	public static double parallelschaltung(double r1, double r2) {	
		double result = ((r1 * r2) / (r1 + r2));
		return result;
	}
}
