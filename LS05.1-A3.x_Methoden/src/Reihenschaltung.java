
public class Reihenschaltung {

	public static void main(String[] args) {
		
		// Berechnung Ersatzwiderstand einer Reihenschaltung
	   double result = reihenschaltung(20.0, 10.0);
	   System.out.println("Der Ersatzwiderstand der Reihenschaltung betr�gt: " + result);
	}
	
	// Methode f�r die Berechnung des Ersatzwiderstands von r1 und r2
	public static double reihenschaltung(double r1, double r2) {
		double result = r1 + r2;
		return result;
	}

}
