﻿import java.util.Scanner;

import javax.security.auth.kerberos.KerberosTicket;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       boolean isRunning = true;
       int ticketKauf = 1;
         
       while(isRunning) {
			
			switch(ticketKauf) {
			
			case 1:
			   double zuZahlenderBetrag = fahrkartenbestellungErfassen();
			       
		       // Geldeinwurf
		       // -----------
		       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	
		       // Fahrscheinausgabe
		       // -----------------
		       fahrkartenAusgeben();
	
		       // Rückgeldberechnung und -Ausgabe
		       // -------------------------------
		       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	
		       // Weiterer Ticketkauf
		     
		       System.out.println("\n\nWollen Sie ein weiteres Ticket kaufen?");
		       System.out.println("1. Ja");
		       System.out.println("2. Nein\n");
		       System.out.print("Ihre Wahl: ");
		       ticketKauf = tastatur.nextInt();
		      
		       break;
		     
			
			case 2:
				System.out.println("\nVielen Dank für Ihren Einkauf!");
				isRunning = false;
				   
				break;
			
			default:
				while(ticketKauf != 1 && ticketKauf != 2) {
				System.out.println(">>falsche Eingabe<<");
				System.out.print("Ihre Wahl: ");
			    ticketKauf = tastatur.nextInt();
				 }
			}
       }       
              
       
    }

    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
        double ticketPreis = 0.0;
        
        int ticketAnzahl = 0;
        int auswahl = 0;
        double ticket = 0.0;
	    double zwischensumme = 0.0;
        
        while (auswahl != 9) {
      
        System.out.println("Fahrkartenbestellvorgang:");
        System.out.println("=========================\n");
        
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");

	    System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
	    System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
	    System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
	    System.out.println("Bezahlen (9)\n");
        
	    System.out.print("Ihre Wahl: ");
        
        auswahl = tastatur.nextInt();
        
        while (auswahl != 1 && auswahl != 2 && auswahl != 3 && auswahl != 9) {
	    	System.out.println(">>falsche Eingabe<<");
	    	System.out.print("Ihre Wahl: ");
	    	auswahl = tastatur.nextInt();
        }
        
	    switch (auswahl) {
	    
	    case 1:
	    	ticketPreis = 2.90;
	    	break;
	    	
	    case 2:
	    	ticketPreis = 8.60;
	    	break;
	    	
	    case 3:
	    	ticketPreis = 23.50;
	    	break;
	    
	    case 9:
	    	return ticket;
	    }    
	    
        System.out.print("Anzahl der Tickets: ");
        ticketAnzahl = tastatur.nextInt();
        
        while(ticketAnzahl < 1 || ticketAnzahl > 10) {
        	System.out.println(">> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
        	System.out.print("Anzahl der Tickets: ");
        	ticketAnzahl = tastatur.nextInt();
        } 
        
        ticket = ticketPreis * ticketAnzahl;
        zwischensumme += ticket;
        ticket = zwischensumme;
        		
        
        System.out.printf("\nZwischensumme: %.2f €\n\n" , ticket);
        }
        
        return ticket;
        
    }
    
	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;

		eingezahlterGesamtbetrag = 0.0;

		   while(eingezahlterGesamtbetrag < zuZahlen)
		   {
			   System.out.printf("Noch zu zahlen: %.2f Euro %n" , (zuZahlen - eingezahlterGesamtbetrag));
			   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			   eingeworfeneMünze = tastatur.nextDouble();
		       eingezahlterGesamtbetrag += eingeworfeneMünze;
		   }
		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		   for (int i = 0; i < 8; i++)
		   {
		      System.out.print("=");
		      warte(250);
		   }
		   System.out.println("\n\n");
	}

	public static void warte(int milisekunde) {
		try {
			Thread.sleep(milisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

	public static double rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag;
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		   if(rückgabebetrag > 0.0)
		   {
			   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO %n" , rückgabebetrag);
			   System.out.println("wird in folgenden Münzen ausgezahlt:");

		       while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
		       {
		    	  muenzeAusgeben(2, "EURO");
		          rückgabebetrag -= 2.0;
		       }
		       while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
		       {
		    	  muenzeAusgeben(1, "EURO");
		          rückgabebetrag -= 1.0;
		       }
		       while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
		       {
		    	  muenzeAusgeben(50, "CENT");
		          rückgabebetrag -= 0.5;
		       }
		       while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
		       {
		    	  muenzeAusgeben(20, "CENT");
		          rückgabebetrag -= 0.2;
		       }
		       while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
		       {
		    	  muenzeAusgeben(10, "CENT");
		          rückgabebetrag -= 0.1;
		       }
		       while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
		       {
		    	  muenzeAusgeben(5, "CENT");
		          rückgabebetrag -= 0.05;
		       }
		       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
		   }
		   return rückgabebetrag;
	}
}